#ifndef GMENU_ACTION_H
#define GMENU_ACTION_H

#include <gtk/gtk.h>

#define GMENU_TYPE_ACTION            (gmenu_action_get_type ())
#define GMENU_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_ACTION, GMenuAction))
#define GMENU_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GMENU_TYPE_ACTION, GMenuActionClass))
#define GMENU_IS_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_ACTION))
#define GMENU_IS_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GMENU_TYPE_ACTION))
#define GMENU_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GMENU_TYPE_ACTION, GMenuActionClass))

typedef struct _GMenuAction      GMenuAction;
typedef struct _GMenuActionClass GMenuActionClass;

struct _GMenuAction
{
  GObject object;

  gchar *name;
  gchar *label;
  gchar *tooltip;

  /* icon */
  gchar *stock_id;

  /* accelerator */
  GQuark accel_quark;

  /* list of proxy widgets */
  GSList *proxies;
};

struct _GMenuActionClass
{
  GObjectClass parent_class;

  /* activation signal */
  void (* activate)            (GMenuAction    *action);

  /* widget creation routines (not signals) */
  GtkWidget *(* create_menu_item)   (GMenuAction *action);
  GtkWidget *(* add_toolbar_button) (GMenuAction *action,
				     GtkToolbar *toolbar);
};

GType gmenu_action_get_type (void);

void       gmenu_action_activate           (GMenuAction *action);
void       gmenu_action_set_sensitive      (GMenuAction *action,
					    gboolean     sensitive);

GtkWidget *gmenu_action_create_icon        (GMenuAction *action,
					    GtkIconSize icon_size);
GtkWidget *gmenu_action_create_menu_item   (GMenuAction *action);
GtkWidget *gmenu_action_add_toolbar_button (GMenuAction *action,
					    GtkToolbar *toolbar);

/* protected ... for use by child actions */
void gmenu_action_block_activate_from   (GMenuAction *action,
					 GtkWidget *proxy);
void gmenu_action_unblock_activate_from (GMenuAction *action,
					 GtkWidget *proxy);

/* protected ... for use by action groups */
void gmenu_action_set_accel_path (GMenuAction *action,
				  const gchar *accel_path);


#endif
