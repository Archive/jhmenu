#include <stdio.h>
#include <gtk/gtk.h>
#include "gmenu-merge.h"

static gchar *node_type_names[] = {
  "UNDECIDED",
  "ROOT",
  "MENUBAR",
  "MENU",
  "TOOLBAR",
  "PLACEHOLDER",
  "POPUPS",
  "MENUITEM",
  "TOOLITEM"
};

static void
print_node(GMenuMerge *merge, GNode *node, gint indent_level)
{
  gint i;
  GMenuMergeNode *mnode;
  GNode *child;

  mnode = node->data;
  for (i = 0; i < indent_level; i++)
    printf("  ");
  printf("%s (%s): action=%s\n", mnode->name, node_type_names[mnode->type],
	 g_quark_to_string(mnode->action_name));

  for (child = node->children; child != NULL; child = child->next) {
    print_node(merge, child, indent_level + 1);
  }
}

int
main(int argc, char **argv)
{
  GMenuMerge *merge;

  gtk_init(&argc, &argv);

  merge = gmenu_merge_new();

  gmenu_merge_add_ui_from_file(merge, "testmerge1.ui", NULL);
  gmenu_merge_add_ui_from_file(merge, "testmerge2.ui", NULL);

  print_node(merge, merge->root_node, 0);

  return 0;
}
