#include <gtk/gtk.h>
#include "gmenu-action.h"
#include "gmenu-action-group.h"
#include "gmenu-action-store.h"
#include "gmenu-action-stack.h"
#include "gmenu-markup.h"

#include "gtk-accel-dialog.h"

#ifndef _
#  define _(String) (String)
#  define N_(String) (String)
#endif

static void
activate_action (GMenuAction *action)
{
  const gchar *name = action->name;
  const gchar *typename = G_OBJECT_TYPE_NAME (action);

  g_message ("Action %s (type=%s) activated", name, typename);
}

static void
toggle_action (GMenuAction *action)
{
  const gchar *name = action->name;
  const gchar *typename = G_OBJECT_TYPE_NAME (action);

  g_message ("Action %s (type=%s) activated (active=%d)", name, typename,
	     (gint) GMENU_TOGGLE_ACTION (action)->active);
}

static void
show_accel_dialog (GMenuAction *action)
{
  GtkWidget *dialog;

  dialog = gtk_accel_dialog_new();
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_object_destroy (GTK_OBJECT (dialog));
}

/* convenience functions for declaring actions */
static GMenuActionStoreEntry entries[] = {
  { "cut", N_("C_ut"), GTK_STOCK_CUT, "<control>X",
    N_("Cut the selected text to the clipboard"),
    G_CALLBACK (activate_action), NULL },
  { "copy", N_("_Copy"), GTK_STOCK_COPY, "<control>C",
    N_("Copy the selected text to the clipboard"),
    G_CALLBACK (activate_action), NULL },
  { "paste", N_("_Paste"), GTK_STOCK_PASTE, "<control>V",
    N_("Paste the text from the clipboard"),
    G_CALLBACK (activate_action), NULL },
  { "bold", N_("_Bold"), GTK_STOCK_BOLD, "<control>B",
    N_("Change to bold face"),
    G_CALLBACK (toggle_action), NULL, TOGGLE_ACTION },

  { "justify-left", N_("_Left"), GTK_STOCK_JUSTIFY_LEFT, "<control>L",
    N_("Left justify the text"),
    G_CALLBACK (toggle_action), NULL, RADIO_ACTION },
  { "justify-center", N_("C_enter"), GTK_STOCK_JUSTIFY_CENTER, "<control>E",
    N_("Center justify the text"),
    G_CALLBACK (toggle_action), NULL, RADIO_ACTION, "justify-left" },
  { "justify-right", N_("_Right"), GTK_STOCK_JUSTIFY_RIGHT, "<control>R",
    N_("Right justify the text"),
    G_CALLBACK (toggle_action), NULL, RADIO_ACTION, "justify-left" },
  { "justify-fill", N_("_Fill"), GTK_STOCK_JUSTIFY_FILL, "<control>J",
    N_("Fill justify the text"),
    G_CALLBACK (toggle_action), NULL, RADIO_ACTION, "justify-left" },
};
static guint n_entries = G_N_ELEMENTS (entries);

static GMenuActionStoreEntry entries2[] = {
  { "quit", N_("_Quit"), GTK_STOCK_QUIT, "<control>Q",
    N_("Quit the application"),
    G_CALLBACK (gtk_main_quit), NULL },
  { "customise-accels", N_("Customise _Accels"), NULL, NULL,
    N_("Customise keyboard shortcuts"),
    G_CALLBACK (show_accel_dialog), NULL },
};
static guint n_entries2 = G_N_ELEMENTS (entries2);

/* XML description of the menus for the test app.  The parser understands
 * a subset of the Bonobo UI XML format, and uses GMarkup for parsing */
static const gchar *ui_info =
"<Root>\n"
"  <menu>\n"
"    <submenu label=\"Menu _1\">\n"
"      <menuitem verb=\"cut\" />\n"
"      <menuitem verb=\"copy\" />\n"
"      <menuitem verb=\"paste\" />\n"
"      <separator />\n"
"      <menuitem verb=\"bold\" />\n"
"      <menuitem verb=\"bold\" />\n"
"      <separator />\n"
"      <menuitem verb=\"quit\" />\n"
"    </submenu>\n"
"    <submenu label=\"Menu _2\">\n"
"      <menuitem verb=\"cut\" />\n"
"      <menuitem verb=\"copy\" />\n"
"      <menuitem verb=\"paste\" />\n"
"      <separator />\n"
"      <menuitem verb=\"bold\" />\n"
"      <separator />\n"
"      <menuitem verb=\"justify-left\" />\n"
"      <menuitem verb=\"justify-center\" />\n"
"      <menuitem verb=\"justify-right\" />\n"
"      <menuitem verb=\"justify-fill\" />\n"
"      <separator />\n"
"      <menuitem verb=\"customise-accels\" />\n"
"    </submenu>\n"
"  </menu>\n"
"  <dockitem name=\"toolbar\">\n"
"    <toolitem verb=\"cut\" />\n"
"    <toolitem verb=\"copy\" />\n"
"    <toolitem verb=\"paste\" />\n"
"    <separator />\n"
"    <toolitem verb=\"bold\" />\n"
"    <separator />\n"
"    <toolitem verb=\"justify-left\" />\n"
"    <toolitem verb=\"justify-center\" />\n"
"    <toolitem verb=\"justify-right\" />\n"
"    <toolitem verb=\"justify-fill\" />\n"
"    <separator />\n"
/* GMenuActionStack also lets you specify which group the action is in */
"    <toolitem verb=\"Global::quit\" />\n"
"  </dockitem>\n"
"</Root>\n";

static void
widget_func(GtkWidget *widget, const gchar *type, const gchar *name,
	    gpointer user_data)
{
  GtkContainer *container = user_data;

  g_message("got widget %s of type %s", name ? name : "(null)", type);
  gtk_container_add(container, widget);
  gtk_widget_show(widget);
}

static void
create_window (GMenuActionGroup *action_group)
{
  GtkWidget *window;
  GtkWidget *box;
  GtkAccelGroup *accel_group;
  GError *error = NULL;

  accel_group = gtk_accel_group_new();

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "Action Test");
  gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
  gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

  box = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), box);
  gtk_widget_show (box);

  if (!gmenu_create_from_string(action_group, widget_func, box,
				accel_group, ui_info, -1, &error))
    {
      g_message ("building menus failed: %s", error->message);
      g_error_free (error);
    }

  g_object_unref(accel_group); /* window holds ref to accel group */

  gtk_widget_show (window);
}

int
main (int argc, char **argv)
{
  GMenuActionStore *action_store;
  GMenuActionStack *action_stack;

  gtk_init (&argc, &argv);

  if (g_file_test("accels", G_FILE_TEST_IS_REGULAR))
    gtk_accel_map_load("accels");

  action_stack = gmenu_action_stack_new("<stack>");

  action_store = gmenu_action_store_new ("Local");
  gmenu_action_store_add_actions (action_store, entries, n_entries);
  gmenu_action_stack_insert_group (action_stack,
				   GMENU_ACTION_GROUP (action_store), -1);
  g_object_unref (action_store);

  action_store = gmenu_action_store_new ("Global");
  gmenu_action_store_add_actions (action_store, entries2, n_entries2);
  gmenu_action_stack_insert_group (action_stack,
				   GMENU_ACTION_GROUP (action_store), -1);
  g_object_unref (action_store);


  create_window (GMENU_ACTION_GROUP (action_stack));

  gtk_main ();

  g_object_unref (action_stack);

  gtk_accel_map_save("accels");

  return 0;
}
