#include "gmenu-radio-action.h"

static void gmenu_radio_action_init       (GMenuRadioAction *action);
static void gmenu_radio_action_class_init (GMenuRadioActionClass *class);

GType
gmenu_radio_action_get_type (void)
{
  static GtkType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuRadioActionClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gmenu_radio_action_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GMenuRadioAction),
        0, /* n_preallocs */
        (GInstanceInitFunc) gmenu_radio_action_init,
      };

      type = g_type_register_static (GMENU_TYPE_TOGGLE_ACTION,
                                     "GMenuRadioAction",
                                     &type_info, 0);
    }
  return type;
}

static void gmenu_radio_action_finalize (GObject *object);
static void gmenu_radio_action_activate (GMenuAction *action);

static GObjectClass *parent_class = NULL;

static void
gmenu_radio_action_class_init (GMenuRadioActionClass *class)
{
  GObjectClass *object_class;
  GMenuActionClass *action_class;

  parent_class = g_type_class_peek_parent (class);
  object_class = G_OBJECT_CLASS (class);
  action_class = GMENU_ACTION_CLASS (class);

  object_class->finalize = gmenu_radio_action_finalize;

  action_class->activate = gmenu_radio_action_activate;
}

static void
gmenu_radio_action_init (GMenuRadioAction *action)
{
  action->group = g_slist_prepend (NULL, action);
}

static void
gmenu_radio_action_finalize (GObject *object)
{
  GMenuRadioAction *action;
  GSList *tmp_list;

  g_return_if_fail (GMENU_IS_RADIO_ACTION (object));

  action = GMENU_RADIO_ACTION (object);

  action->group = g_slist_remove (action->group, action);

  tmp_list = action->group;

  while (tmp_list)
    {
      GMenuRadioAction *tmp_action = tmp_list->data;

      tmp_list = tmp_list->next;
      tmp_action->group = action->group;
    }

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static void
gmenu_radio_action_activate (GMenuAction *action)
{
  GMenuRadioAction *radio_action;
  GMenuToggleAction *toggle_action;
  GMenuToggleAction *tmp_action;
  GSList *tmp_list;
  gboolean toggled = FALSE;

  g_return_if_fail (GMENU_IS_RADIO_ACTION (action));

  radio_action = GMENU_RADIO_ACTION (action);
  toggle_action = GMENU_TOGGLE_ACTION (action);

  if (toggle_action->active)
    {
      tmp_action = NULL;
      tmp_list = radio_action->group;

      while (tmp_list)
	{
	  tmp_action = tmp_list->data;
	  tmp_list = tmp_list->next;

	  if (tmp_action->active && (tmp_action != toggle_action))
	    break;

	  tmp_action = NULL;
	}

      if (tmp_action)
	{
	  toggled = TRUE;
	  toggle_action->active = !toggle_action->active;
	}
    }
  else
    {
      toggled = TRUE;
      toggle_action->active = !toggle_action->active;

      tmp_list = radio_action->group;
      while (tmp_list)
	{
	  tmp_action = tmp_list->data;
	  tmp_list = tmp_list->next;

	  if (tmp_action->active && (tmp_action != toggle_action))
	    {
	      gmenu_action_activate (GMENU_ACTION (tmp_action));
	      break;
	    }
	}
    }

  if (toggled)
    gmenu_toggle_action_toggled (toggle_action);
}

GSList *
gmenu_radio_action_get_group (GMenuRadioAction *action)
{
  g_return_val_if_fail (GMENU_IS_RADIO_ACTION (action), NULL);

  return action->group;
}

void
gmenu_radio_action_set_group (GMenuRadioAction *action, GSList *group)
{
  g_return_if_fail (GMENU_IS_RADIO_ACTION (action));
  g_return_if_fail (!g_slist_find (group, action));

  if (action->group)
    {
      GSList *slist;

      action->group = g_slist_remove (action->group, action);

      for (slist = action->group; slist; slist = slist->next)
	{
	  GMenuRadioAction *tmp_action = slist->data;

	  tmp_action->group = action->group;
	}
    }

  action->group = g_slist_prepend (group, action);

  if (group)
    {
      GSList *slist;

      for (slist = action->group; slist; slist = slist->next)
	{
	  GMenuRadioAction *tmp_action = slist->data;

	  tmp_action->group = action->group;
	}
    }
  else
    {
      GMENU_TOGGLE_ACTION (action)->active = TRUE;
    }
}
