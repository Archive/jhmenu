#include <string.h>
#include <gmenu-merge.h>

#ifndef _
#  define _(string) (string)
#endif

#define NODE_INFO(node) ((GMenuMergeNode *)node->data)

static void   gmenu_merge_class_init    (GMenuMergeClass *class);
static void   gmenu_merge_init          (GMenuMerge *merge);

static void   gmenu_merge_queue_update  (GMenuMerge *self);
static void   gmenu_merge_ensure_update (GMenuMerge *self);
static void   gmenu_merge_dirty_all     (GMenuMerge *self);

static GNode *get_child_node            (GMenuMerge *self, GNode *parent,
					 const gchar *childname,
					 gint childname_length,
					 GMenuMergeNodeType node_type,
					 gboolean create, gboolean top);
static GNode *gmenu_merge_get_node      (GMenuMerge *self, const gchar *path,
					 GMenuMergeNodeType node_type,
					 gboolean create);

static GMemChunk *merge_node_chunk = NULL;

GType
gmenu_merge_get_type (void)
{
  static GtkType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuMergeClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gmenu_merge_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GMenuMerge),
        0, /* n_preallocs */
        (GInstanceInitFunc) gmenu_merge_init,
      };

      type = g_type_register_static (G_TYPE_OBJECT,
				     "GMenuMerge",
				     &type_info, 0);
    }
  return type;
}

static void
gmenu_merge_class_init (GMenuMergeClass *class)
{
  if (!merge_node_chunk)
    merge_node_chunk = g_mem_chunk_create(GMenuMergeNode, 64,
					  G_ALLOC_AND_FREE);
}


static void
gmenu_merge_init (GMenuMerge *self)
{
  self->accel_group = gtk_accel_group_new();

  self->root_node = NULL;
  self->action_groups = NULL;

  get_child_node(self, NULL, "Root", 4, GMENU_MERGE_ROOT, TRUE, FALSE);
  get_child_node(self, self->root_node, "popups", 6, GMENU_MERGE_POPUPS, TRUE, FALSE);
}

GMenuMerge *
gmenu_merge_new (void)
{
  return g_object_new(GMENU_TYPE_MERGE, NULL);
}

void
gmenu_merge_insert_action_group (GMenuMerge *self,
				 GMenuActionGroup *action_group, gint pos)
{
  g_return_if_fail (GMENU_IS_MERGE(self));
  g_return_if_fail (GMENU_IS_ACTION_GROUP(action_group));
  g_return_if_fail (g_list_find(self->action_groups, action_group) == NULL);

  g_object_ref(action_group);
  self->action_groups = g_list_insert(self->action_groups, action_group, pos);

  /* dirty all nodes, as action bindings may change */
  gmenu_merge_dirty_all(self);
}

void
gmenu_merge_remove_action_group (GMenuMerge *self,
				 GMenuActionGroup *action_group)
{
  g_return_if_fail (GMENU_IS_MERGE(self));
  g_return_if_fail (GMENU_IS_ACTION_GROUP(action_group));
  g_return_if_fail (g_list_find(self->action_groups, action_group) != NULL);

  self->action_groups = g_list_remove(self->action_groups, action_group);
  g_object_unref(action_group);

  /* dirty all nodes, as action bindings may change */
  gmenu_merge_dirty_all(self);
}

GtkWidget *
gmenu_merge_get_widget (GMenuMerge *self, const gchar *path)
{
  GNode *node;

  /* ensure that there are no pending updates before we get the
   * widget */
  gmenu_merge_ensure_update(self);

  node = gmenu_merge_get_node(self, path, GMENU_MERGE_UNDECIDED, FALSE);
  return NODE_INFO(node)->proxy;
}

static GNode *
get_child_node(GMenuMerge *self, GNode *parent,
	       const gchar *childname, gint childname_length,
	       GMenuMergeNodeType node_type,
	       gboolean create, gboolean top)
{
  GNode *child = NULL;

  g_return_val_if_fail(parent == NULL ||
	       (NODE_INFO(parent)->type != GMENU_MERGE_MENUITEM &&
		NODE_INFO(parent)->type != GMENU_MERGE_TOOLITEM), NULL);

  if (parent)
    {
      if (childname)
	{
	  for (child = parent->children; child != NULL; child = child->next)
	    {
	      if (!strncmp(NODE_INFO(child)->name, childname, childname_length))
		{
		  /* if undecided about node type, set it */
		  if (NODE_INFO(child)->type == GMENU_MERGE_UNDECIDED)
		    NODE_INFO(child)->type = node_type;
		  
		  /* warn about type mismatch */
		  if (NODE_INFO(child)->type != GMENU_MERGE_UNDECIDED &&
		      NODE_INFO(child)->type != node_type)
		    g_warning("node type doesn't match %d", node_type);
		  
		  return child;
		}
	    }
	}
      if (!child && create)
	{
	  GMenuMergeNode *mnode;

	  mnode = g_chunk_new0(GMenuMergeNode, merge_node_chunk);
	  mnode->type = node_type;
	  mnode->name = g_strndup(childname, childname_length);
	  mnode->dirty = TRUE;

	  if (top)
	    child = g_node_prepend_data(parent, mnode);
	  else
	    child = g_node_append_data(parent, mnode);
	}
    }
  else
    {
      /* handle root node */
      if (self->root_node)
	{
	  child = self->root_node;
	  if (strncmp(NODE_INFO(child)->name, childname, childname_length) !=0)
	    g_warning("root node name '%s' doesn't match '%s'",
		      childname, NODE_INFO(child)->name);
	  if (NODE_INFO(child)->type != GMENU_MERGE_ROOT)
	    g_warning("base element must be of type ROOT");
	}
      else if (create)
	{
	  GMenuMergeNode *mnode;

	  mnode = g_chunk_new0(GMenuMergeNode, merge_node_chunk);
	  mnode->type = node_type;
	  mnode->name = g_strndup(childname, childname_length);
	  mnode->dirty = TRUE;
	  
	  child = self->root_node = g_node_new(mnode);
	}
    }

  return child;
}

static GNode *
gmenu_merge_get_node(GMenuMerge *self, const gchar *path,
		     GMenuMergeNodeType node_type, gboolean create)
{
  const gchar *pos, *end;
  GNode *parent, *node;

  end = path + strlen(path);
  pos = path;
  parent = node = NULL;
  while (pos < end)
    {
      const gchar *slash;
      gsize length;

      slash = strchr(pos, '/');
      if (slash)
	length = slash - pos;
      else
	length = strlen(pos);

      node = get_child_node(self, parent, pos, length, GMENU_MERGE_UNDECIDED,
			    create, FALSE);
      if (!node)
	return NULL;

      pos += length + 1; /* move past the node name and the slash too */
      parent = node;
    }

  if (NODE_INFO(node)->type == GMENU_MERGE_UNDECIDED)
    NODE_INFO(node)->type = node_type;
  return node;
}


/* -------------------- The UI file parser -------------------- */

typedef enum {
  STATE_START,
  STATE_ROOT,
  STATE_MENU,
  STATE_TOOLBAR,
  STATE_POPUPS,
  STATE_MENUITEM,
  STATE_TOOLITEM,
  STATE_END
} ParseState;

typedef struct _ParseContext ParseContext;
struct _ParseContext
{
  ParseState state;
  ParseState prev_state;

  GMenuMerge *self;

  GNode *current;
};

static void
start_element_handler (GMarkupParseContext *context,
		       const gchar *element_name,
		       const gchar **attribute_names,
		       const gchar **attribute_values,
		       gpointer user_data,
		       GError **error)
{
  ParseContext *ctx = user_data;
  GMenuMerge *self = ctx->self;

  gint i;
  const gchar *node_name;
  GQuark verb_quark;
  gboolean top;

  gboolean raise_error = TRUE;
  gchar *error_attr = NULL;

  //g_message("starting element %s", element_name);

  /* work out a name for this node.  Either the name attribute, or
   * element name */
  node_name = element_name;
  verb_quark = 0;
  top = FALSE;
  for (i = 0; attribute_names[i] != NULL; i++)
    {
      if (!strcmp(attribute_names[i], "name"))
	{
	  node_name = attribute_values[i];
	}
      else if (!strcmp(attribute_names[i], "verb"))
	{
	  verb_quark = g_quark_from_string(attribute_values[i]);
	}
      else if (!strcmp(attribute_names[i], "pos"))
	{
	  top = !strcmp(attribute_values[i], "top");
	}
    }
  /* if no verb, then set it to the node's name */
  if (verb_quark == 0)
    verb_quark = g_quark_from_string(node_name);

  switch (element_name[0])
    {
    case 'R':
      if (ctx->state == STATE_START && !strcmp(element_name, "Root"))
	{
	  ctx->state = STATE_ROOT;
	  ctx->current = self->root_node;
	  raise_error = FALSE;
	}
      break;
    case 'm':
      if (ctx->state == STATE_ROOT && !strcmp(element_name, "menu"))
	{
	  ctx->state = STATE_MENU;
	  ctx->current = get_child_node(self, ctx->current,
					node_name, strlen(node_name),
					GMENU_MERGE_MENUBAR,
					TRUE, FALSE);
	  if (NODE_INFO(ctx->current)->action_name == 0)
	    NODE_INFO(ctx->current)->action_name = verb_quark;
	  NODE_INFO(ctx->current)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      else if (ctx->state == STATE_MENU && !strcmp(element_name, "menuitem"))
	{
	  GNode *node;

	  ctx->state = STATE_MENUITEM;
	  node = get_child_node(self, ctx->current,
				node_name, strlen(node_name),
				GMENU_MERGE_MENUITEM,
				TRUE, top);
	  if (NODE_INFO(node)->action_name == 0)
	    NODE_INFO(node)->action_name = verb_quark;
	  NODE_INFO(node)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      break;
    case 'd':
      if (ctx->state == STATE_ROOT && !strcmp(element_name, "dockitem"))
	{
	  ctx->state = STATE_TOOLBAR;
	  ctx->current = get_child_node(self, ctx->current,
					node_name, strlen(node_name),
					GMENU_MERGE_TOOLBAR,
					TRUE, FALSE);
	  if (NODE_INFO(ctx->current)->action_name == 0)
	    NODE_INFO(ctx->current)->action_name = verb_quark;
	  NODE_INFO(ctx->current)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      break;
    case 'p':
      if (ctx->state == STATE_ROOT && !strcmp(element_name, "popups"))
	{
	  ctx->state = STATE_POPUPS;
	  ctx->current = get_child_node(self, ctx->current,
					node_name, strlen(node_name),
					GMENU_MERGE_POPUPS,
					TRUE, FALSE);
	  NODE_INFO(ctx->current)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      else if (ctx->state == STATE_POPUPS && !strcmp(element_name, "popup"))
	{
	  ctx->state = STATE_MENU;
	  ctx->current = get_child_node(self, ctx->current,
					node_name, strlen(node_name),
					GMENU_MERGE_MENU,
					TRUE, FALSE);
	  if (NODE_INFO(ctx->current)->action_name == 0)
	    NODE_INFO(ctx->current)->action_name = verb_quark;
	  NODE_INFO(ctx->current)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      else if ((ctx->state == STATE_MENU || ctx->state == STATE_TOOLBAR) &&
	       !strcmp(element_name, "placeholder"))
	{
	  ctx->current = get_child_node(self, ctx->current,
					node_name, strlen(node_name),
					GMENU_MERGE_PLACEHOLDER,
					TRUE, top);
	  NODE_INFO(ctx->current)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      break;
    case 's':
      if (ctx->state == STATE_MENU && !strcmp(element_name, "submenu"))
	{
	  ctx->state = STATE_MENU;
	  ctx->current = get_child_node(self, ctx->current,
					node_name, strlen(node_name),
					GMENU_MERGE_MENU,
					TRUE, top);
	  if (NODE_INFO(ctx->current)->action_name == 0)
	    NODE_INFO(ctx->current)->action_name = verb_quark;
	  NODE_INFO(ctx->current)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      else if ((ctx->state == STATE_MENU || ctx->state == STATE_TOOLBAR) &&
	       !strcmp(element_name, "separator"))
	{
	  raise_error = FALSE;
	}
      break;
    case 't':
      if (ctx->state == STATE_TOOLBAR && !strcmp(element_name, "toolitem"))
	{
	  GNode *node;

	  ctx->state = STATE_TOOLITEM;
	  node = get_child_node(self, ctx->current,
				node_name, strlen(node_name),
				GMENU_MERGE_TOOLITEM,
				TRUE, top);
	  if (NODE_INFO(node)->action_name == 0)
	    NODE_INFO(node)->action_name = verb_quark;
	  NODE_INFO(node)->dirty = TRUE;

	  /* this should be adding to a list of actions, along with a
	     tag for this UI file */

	  raise_error = FALSE;
	}
      break;
    default:
      break;
    }
  if (raise_error)
    {
      gint line_number, char_number;
 
      g_markup_parse_context_get_position (context,
					   &line_number, &char_number);
      if (error_attr)
	g_set_error (error,
		     G_MARKUP_ERROR,
		     G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
		     _("Unknown attribute '%s' on line %d char %d"),
		     error_attr,
		     line_number, char_number);
      else
	g_set_error (error,
		     G_MARKUP_ERROR,
		     G_MARKUP_ERROR_UNKNOWN_ELEMENT,
		     _("Unknown tag '%s' on line %d char %d"),
		     element_name,
		     line_number, char_number);
    }
}

static void
end_element_handler (GMarkupParseContext *context,
		     const gchar *element_name,
		     gpointer user_data,
		     GError **error)
{
  ParseContext *ctx = user_data;
  GMenuMerge *self = ctx->self;

  //g_message("ending element %s (state=%d)", element_name, ctx->state);

  switch (ctx->state)
    {
    case STATE_START:
      g_warning("shouldn't get any end tags in start state");
      /* should we GError here? */
      break;
    case STATE_ROOT:
      if (ctx->current != self->root_node)
	g_warning("we are in STATE_ROOT, but the current node isn't the root");
      ctx->current = NULL;
      ctx->state = STATE_END;
      break;
    case STATE_MENU:
      ctx->current = ctx->current->parent;
      if (NODE_INFO(ctx->current)->type == GMENU_MERGE_ROOT) /* menubar */
	ctx->state = STATE_ROOT;
      else if (NODE_INFO(ctx->current)->type == GMENU_MERGE_POPUPS) /* popup */
	ctx->state = STATE_POPUPS;
      /* else, stay in STATE_MENU state */
      break;
    case STATE_TOOLBAR:
      ctx->current = ctx->current->parent;
      /* we conditionalise this test, in case we are closing off a
       * placeholder */
      if (NODE_INFO(ctx->current)->type == GMENU_MERGE_ROOT)
	ctx->state = STATE_ROOT;
      break;
    case STATE_POPUPS:
      ctx->current = ctx->current->parent;
      ctx->state = STATE_ROOT;
      break;
    case STATE_MENUITEM:
      ctx->state = STATE_MENU;
      break;
    case STATE_TOOLITEM:
      ctx->state = STATE_TOOLBAR;
      break;
    case STATE_END:
      g_warning("shouldn't get any end tags at this point");
      /* should do an error here */
      break;
    }
}

static void
cleanup (GMarkupParseContext *context,
	 GError *error,
	 gpointer user_data)
{
  ParseContext *ctx = user_data;
  GMenuMerge *self = ctx->self;

  ctx->current = NULL;
  /* should also walk through the tree and get rid of nodes related to
   * this UI file's tag */
}

static GMarkupParser ui_parser = {
  start_element_handler,
  end_element_handler,
  NULL,
  NULL,
  cleanup
};

gboolean
gmenu_merge_add_ui_from_string (GMenuMerge *self,
				const gchar *buffer, guint length,
				GError **error)
{
  ParseContext ctx = { 0 };
  GMarkupParseContext *context;
  gboolean res = TRUE;

  g_return_val_if_fail(GMENU_IS_MERGE(self), FALSE);
  g_return_val_if_fail(buffer != NULL, FALSE);

  ctx.state = STATE_START;
  ctx.self = self;
  ctx.current = NULL;

  context = g_markup_parse_context_new(&ui_parser, 0, &ctx, NULL);
  if (length < 0)
    length = strlen(buffer);

  if (g_markup_parse_context_parse(context, buffer, length, error))
    {
      if (!g_markup_parse_context_end_parse(context, error))
	res = FALSE;
    }
  else
    res = FALSE;

  g_markup_parse_context_free (context);

  gmenu_merge_queue_update(self);

  return res;
}

gboolean
gmenu_merge_add_ui_from_file (GMenuMerge *self,
			      const gchar *filename,
			      GError **error)
{
  gchar *buffer;
  gint length;
  gboolean res;

  if (!g_file_get_contents (filename, &buffer, &length, error))
    return FALSE;

  res = gmenu_merge_add_ui_from_string(self, buffer, length, error);
  g_free(buffer);

  return res;
}


/* -------------------- Updates -------------------- */

static gboolean
do_updates(GMenuMerge *self)
{
  /* this function needs to check through the tree for dirty nodes.
   * For such nodes, it needs to do the following:
   *
   * 1) check if they are referenced by any loaded UI files anymore.
   *    In which case, the proxy widget should be destroyed, unless
   *    there are any subnodes.
   *
   * 2) lookup the action for this node again.  If it is different to
   *    the current one (or if no previous action has been looked up),
   *    the proxy is reconnected to the new action (or a new proxy widget
   *    is created and added to the parent container).
   */

  self->update_tag = 0;
  return FALSE;
}

static void
gmenu_merge_queue_update (GMenuMerge *self)
{
  if (self->update_tag != 0)
    return;

  self->update_tag = g_idle_add((GSourceFunc)do_updates, self);
}

static void
gmenu_merge_ensure_update (GMenuMerge *self)
{
  if (self->update_tag != 0)
    {
      g_source_remove(self->update_tag);
      do_updates(self);
    }
}

static gboolean
dirty_traverse_func (GNode *node, gpointer data)
{
  NODE_INFO(node)->dirty = TRUE;
  return FALSE;
}

static void
gmenu_merge_dirty_all (GMenuMerge *self)
{
  g_node_traverse(self->root_node, G_PRE_ORDER, G_TRAVERSE_ALL, -1,
		  dirty_traverse_func, NULL);
  gmenu_merge_queue_update(self);
}
