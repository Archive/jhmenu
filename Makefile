
CC = gcc

CFLAGS := $(shell pkg-config --cflags gtk+-2.0) -Wall -Wmissing-prototypes -O2 -g
LIBS   := $(shell pkg-config --libs   gtk+-2.0)

LIB_SOURCES = gmenu-action.c gmenu-toggle-action.c gmenu-radio-action.c \
  gmenu-action-group.c gmenu-action-store.c gmenu-action-stack.c \
  gmenu-markup.c gmenu-merge.c gtk-accel-dialog.c
LIB_OBJS = $(LIB_SOURCES:%.c=%.lo)

all: libgmenuaction.la tester testmerge

libgmenuaction.la: $(LIB_OBJS)
	libtool --mode=link $(CC) -o $@ $(LIB_OBJS) $(LIBS)

tester: tester.o libgmenuaction.la
	libtool --mode=link $(CC) -o $@ tester.o libgmenuaction.la

testmerge: testmerge.o libgmenuaction.la
	libtool --mode=link $(CC) -o $@ testmerge.o libgmenuaction.la

dist:
	ln -sf . jhmenu
	tar czf jhmenu.tar.gz jhmenu/Makefile gmenu/*.[ch]
	rm -f jhmenu

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
%.lo: %.c
	libtool --mode=compile $(CC) $(CFLAGS) -c $< -o $@
