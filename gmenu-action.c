#include "gmenu-action.h"

#ifndef _
# define _(s) (s)
#endif

enum {
  ACTIVATE,
  LAST_SIGNAL
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_LABEL,
  PROP_TOOLTIP,
  PROP_STOCK,
};

static void gmenu_action_init       (GMenuAction *action);
static void gmenu_action_class_init (GMenuActionClass *class);

static GQuark       accel_path_id  = 0;
static const gchar *accel_path_key = "GMenuAction::accel_path";

GType
gmenu_action_get_type (void)
{
  static GtkType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuActionClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gmenu_action_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GMenuAction),
        0, /* n_preallocs */
        (GInstanceInitFunc) gmenu_action_init,
      };

      type = g_type_register_static (G_TYPE_OBJECT,
				     "GMenuAction",
				     &type_info, 0);
    }
  return type;
}

static void gmenu_action_finalize     (GObject *object);
static void gmenu_action_set_property (GObject         *object,
				       guint            prop_id,
				       const GValue    *value,
				       GParamSpec      *pspec);
static void gmenu_action_get_property (GObject         *object,
				       guint            prop_id,
				       GValue          *value,
				       GParamSpec      *pspec);

static GtkWidget *create_menu_item    (GMenuAction *action);
static GtkWidget *add_toolbar_button  (GMenuAction *action,
				       GtkToolbar *toolbar);


static GObjectClass *parent_class = NULL;
static guint         action_signals[LAST_SIGNAL] = { 0 };


static void
gmenu_action_class_init (GMenuActionClass *class)
{
  GObjectClass *object_class;

  accel_path_id = g_quark_from_static_string(accel_path_key);

  parent_class = g_type_class_peek_parent (class);
  object_class = G_OBJECT_CLASS(class);

  object_class->finalize     = gmenu_action_finalize;
  object_class->set_property = gmenu_action_set_property;
  object_class->get_property = gmenu_action_get_property;

  class->activate = NULL;

  class->create_menu_item = create_menu_item;
  class->add_toolbar_button = add_toolbar_button;

  g_object_class_install_property (object_class,
				   PROP_NAME,
				   g_param_spec_string ("name",
							_("Name"),
							_("A unique name for the action."),
							NULL,
							G_PARAM_READWRITE |
							G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class,
				   PROP_LABEL,
				   g_param_spec_string ("label",
							_("Label"),
							_("The label used for menu items and buttons that activate this action."),
							NULL,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_TOOLTIP,
				   g_param_spec_string ("tooltip",
							_("Tooltip"),
							_("A tooltip for this action."),
							NULL,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_STOCK,
				   g_param_spec_string ("stock",
							_("Stock Icon"),
							_("The stock icon displayed in widgets representing this action."),
							NULL,
							G_PARAM_READWRITE));

  action_signals[ACTIVATE] =
    g_signal_new ("activate",
		  G_OBJECT_CLASS_TYPE (class),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_NO_RECURSE,
		  G_STRUCT_OFFSET (GMenuActionClass, activate),  NULL, NULL,
		  gtk_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);
}


static void
gmenu_action_init (GMenuAction *action)
{
  action->name = NULL;
  action->label = NULL;
  action->tooltip = NULL;
  action->stock_id = NULL;

  action->accel_quark = 0;

  action->proxies = NULL;
}

static void
gmenu_action_finalize (GObject *object)
{
  GMenuAction *action;

  action = GMENU_ACTION (object);

  g_free (action->name);
  g_free (action->label);
  g_free (action->tooltip);
  g_free (action->stock_id);
}

static void
gmenu_action_set_property (GObject         *object,
			   guint            prop_id,
			   const GValue    *value,
			   GParamSpec      *pspec)
{
  GMenuAction *action;

  action = GMENU_ACTION (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_free (action->name);
      action->name = g_value_dup_string (value);
      break;
    case PROP_LABEL:
      g_free (action->label);
      action->label = g_value_dup_string (value);
      break;
    case PROP_TOOLTIP:
      g_free (action->tooltip);
      action->tooltip = g_value_dup_string (value);
      break;
    case PROP_STOCK:
      g_free (action->stock_id);
      action->stock_id = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gmenu_action_get_property (GObject         *object,
			   guint            prop_id,
			   GValue    *value,
			   GParamSpec      *pspec)
{
  GMenuAction *action;

  action = GMENU_ACTION (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, action->name);
      break;
    case PROP_LABEL:
      g_value_set_string (value, action->label);
      break;
    case PROP_TOOLTIP:
      g_value_set_string (value, action->tooltip);
      break;
    case PROP_STOCK:
      g_value_set_string (value, action->stock_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static GtkWidget *
create_menu_item (GMenuAction *action)
{
  GtkWidget *menu_item;
  GtkWidget *icon;
  GtkWidget *label;

  icon = gmenu_action_create_icon (action, GTK_ICON_SIZE_MENU);
  if (icon) {
    menu_item = gtk_image_menu_item_new ();
    gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menu_item), icon);
    gtk_widget_show (icon);
  } else {
    menu_item = gtk_menu_item_new ();
  }

  label = g_object_new (GTK_TYPE_ACCEL_LABEL,
			"label", action->label,
			"use_underline", TRUE,
			"xalign", 0.0,
			"visible", TRUE,
			"parent", menu_item,
			"accel_widget", menu_item,
			NULL);
  
  gtk_widget_show (menu_item);
  return menu_item;
}

static void
set_use_underline (GtkWidget *widget, GtkWidget *button)
{
  if (GTK_IS_LABEL (widget)) {
    gtk_label_set_use_underline (GTK_LABEL (widget), TRUE);
    gtk_label_set_mnemonic_widget (GTK_LABEL (widget), button);
  }
}

static GtkWidget *
add_toolbar_button (GMenuAction *action, GtkToolbar *toolbar)
{
  GtkWidget *icon;
  GtkWidget *button;

  icon = gmenu_action_create_icon (action, GTK_ICON_SIZE_SMALL_TOOLBAR);

  button = gtk_toolbar_append_item (toolbar, action->label, action->tooltip,
				    NULL, icon, NULL, NULL);

  /* make the label in the toolbar use a mnemonic */
  gtk_container_foreach (GTK_CONTAINER(GTK_BIN (button)->child),
			 (GtkCallback) set_use_underline, button);
  return button;
}

void
gmenu_action_activate (GMenuAction *action)
{
  g_signal_emit (action, action_signals[ACTIVATE], 0);
}

void
gmenu_action_set_sensitive (GMenuAction *action,
			    gboolean     sensitive)
{
  GSList *list;

  for (list = action->proxies; list; list = list->next)
    {
      GtkWidget *widget = GTK_WIDGET (list->data);

      gtk_widget_set_sensitive (widget, sensitive);
    }
}

GtkWidget *
gmenu_action_create_icon (GMenuAction *action, GtkIconSize icon_size)
{
  if (action->stock_id)
    return gtk_image_new_from_stock (action->stock_id, icon_size);
  else
    return NULL;
}

static void
gmenu_action_remove_proxy (GtkWidget *widget, GMenuAction *action)
{
  action->proxies = g_slist_remove (action->proxies, widget);
}

GtkWidget *
gmenu_action_create_menu_item (GMenuAction *action)
{
  GtkWidget *menu_item;

  menu_item = (* GMENU_ACTION_GET_CLASS (action)->create_menu_item) (action);

  g_object_ref (action);
  g_object_set_data_full (G_OBJECT (menu_item), "gmenu-action",
			  action, g_object_unref);

  /* connect up the action signal ... */
  g_signal_connect_object (menu_item, "activate",
			   G_CALLBACK (gmenu_action_activate), action,
			   G_CONNECT_SWAPPED);

  /* add this widget to the list of proxies */
  action->proxies = g_slist_prepend (action->proxies, menu_item);
  g_signal_connect (menu_item, "destroy",
		    G_CALLBACK (gmenu_action_remove_proxy), action);

  /* set the accel_quark for changeable menus */
  if (action->accel_quark)
    {
      gtk_menu_item_set_accel_path (GTK_MENU_ITEM (menu_item),
				    g_quark_to_string (action->accel_quark));
    }

  return menu_item;
}

GtkWidget *
gmenu_action_add_toolbar_button (GMenuAction *action, GtkToolbar *toolbar)
{
  GtkWidget *button;

  button =
    (* GMENU_ACTION_GET_CLASS (action)->add_toolbar_button) (action, toolbar);

  g_object_ref (action);
  g_object_set_data_full (G_OBJECT (button), "gmenu-action",
			  action, g_object_unref);

  /* connect up the action signal ... */
  g_signal_connect_object (button, "clicked",
			   G_CALLBACK (gmenu_action_activate), action,
			   G_CONNECT_SWAPPED);

  /* add this widget to the list of proxies */
  action->proxies = g_slist_prepend (action->proxies, button);
  g_signal_connect (button, "destroy",
		    G_CALLBACK (gmenu_action_remove_proxy), action);

  return button;
}

void
gmenu_action_block_activate_from (GMenuAction *action, GtkWidget *proxy)
{
  g_return_if_fail (GMENU_IS_ACTION (action));
  
  g_signal_handlers_block_by_func (proxy, G_CALLBACK (gmenu_action_activate),
				   action);
}

void
gmenu_action_unblock_activate_from (GMenuAction *action, GtkWidget *proxy)
{
  g_return_if_fail (GMENU_IS_ACTION (action));

  g_signal_handlers_unblock_by_func (proxy, G_CALLBACK (gmenu_action_activate),
				     action);
}

void
gmenu_action_set_accel_path (GMenuAction *action, const gchar *accel_path)
{
  action->accel_quark = g_quark_from_string(accel_path);
}
