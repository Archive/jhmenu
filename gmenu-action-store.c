#include "gmenu-action-store.h"

#ifndef _
# define _(s) (s)
#endif

static void gmenu_action_store_init       (GMenuActionStore *action_store);
static void gmenu_action_store_class_init (GMenuActionStoreClass *class);
static void gmenu_action_store_group_init (GMenuActionGroupClass *vtable);

GType
gmenu_action_store_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuActionStoreClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gmenu_action_store_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GMenuActionStore),
        0, /* n_preallocs */
        (GInstanceInitFunc) gmenu_action_store_init,
      };
      static const GInterfaceInfo group_info =
      {
        (GInterfaceInitFunc) gmenu_action_store_group_init,
        (GInterfaceFinalizeFunc) NULL,
        NULL,
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GMenuActionStore",
				     &type_info, 0);
      g_type_add_interface_static (type, GMENU_TYPE_ACTION_GROUP, &group_info);
    }

  return type;
}

static void         gmenu_action_store_finalize   (GObject *object);
static const gchar *gmenu_action_store_get_name   (GMenuActionGroup *group);
static GMenuAction *gmenu_action_store_get_action (GMenuActionGroup *group,
						   const gchar *name);
static GObjectClass *parent_class = NULL;

static void
gmenu_action_store_class_init (GMenuActionStoreClass *class)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (class);
  parent_class = g_type_class_peek_parent (class);

  object_class->finalize = gmenu_action_store_finalize;
}

static void
gmenu_action_store_group_init (GMenuActionGroupClass *vtable)
{
  vtable->get_name   = gmenu_action_store_get_name;
  vtable->get_action = gmenu_action_store_get_action;
}

static void
gmenu_action_store_init (GMenuActionStore *action_store)
{
  action_store->name = NULL;
  action_store->actions = g_hash_table_new_full (g_str_hash, g_str_equal,
		(GDestroyNotify) g_free, (GDestroyNotify) g_object_unref);
}

static void
gmenu_action_store_finalize (GObject *object)
{
  GMenuActionStore *action_store;

  action_store = GMENU_ACTION_STORE (object);

  g_free (action_store->name);
  action_store->name = NULL;

  g_hash_table_destroy (action_store->actions);
  action_store->actions = NULL;

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static const gchar *
gmenu_action_store_get_name (GMenuActionGroup *group)
{
  GMenuActionStore *action_store;

  action_store = GMENU_ACTION_STORE (group);

  return action_store->name;
}

static GMenuAction *
gmenu_action_store_get_action (GMenuActionGroup *group, const gchar *name)
{
  GMenuActionStore *action_store;

  action_store = GMENU_ACTION_STORE (group);

  return g_hash_table_lookup (action_store->actions, name);
}

GMenuActionStore *
gmenu_action_store_new (const gchar *name)
{
  GMenuActionStore *action_store = g_object_new (GMENU_TYPE_ACTION_STORE,
						 NULL);

  action_store->name = g_strdup (name);

  return action_store;
}

void
gmenu_action_store_add_action (GMenuActionStore *store,
			       GMenuAction *action)
{
  g_return_if_fail (GMENU_IS_ACTION_STORE (store));
  g_return_if_fail (GMENU_IS_ACTION (action));
  g_return_if_fail (action->name != NULL);

  g_hash_table_insert (store->actions, g_strdup (action->name),
		       g_object_ref (action));
}

void
gmenu_action_store_remove_action (GMenuActionStore *store,
				  GMenuAction *action)
{
  g_return_if_fail (GMENU_IS_ACTION_STORE (store));
  g_return_if_fail (GMENU_IS_ACTION (action));
  g_return_if_fail (action->name != NULL);

  /* extra protection to make sure action->name is valid */
  g_object_ref (action);
  g_hash_table_remove (store->actions, action->name);
  g_object_unref (action);
}

void
gmenu_action_store_add_actions (GMenuActionStore *store,
				GMenuActionStoreEntry *entries,
				guint n_entries)
{
  guint i;

  for (i = 0; i < n_entries; i++)
    {
      GMenuAction *action;
      GType action_type;
      gchar *accel_path;

      switch (entries[i].entry_type) {
      case NORMAL_ACTION:
	action_type = GMENU_TYPE_ACTION;
	break;
      case TOGGLE_ACTION:
	action_type = GMENU_TYPE_TOGGLE_ACTION;
	break;
      case RADIO_ACTION:
	action_type = GMENU_TYPE_RADIO_ACTION;
	break;
      default:
	g_warning ("unsupported action type");
	action_type = GMENU_TYPE_ACTION;
      }

      action = g_object_new (action_type,
			     "name", entries[i].name,
			     "label", _(entries[i].label),
			     "tooltip", _(entries[i].tooltip),
			     "stock", entries[i].stock_id,
			     NULL);

      if (entries[i].entry_type == RADIO_ACTION &&
	  entries[i].extra_data != NULL)
	{
	  GMenuAction *radio_action;
	  GSList *group;

	  radio_action =
	    gmenu_action_group_get_action (GMENU_ACTION_GROUP (store),
					   entries[i].extra_data);
	  if (radio_action)
	    {
	      group = gmenu_radio_action_get_group (GMENU_RADIO_ACTION (radio_action));
	      gmenu_radio_action_set_group (GMENU_RADIO_ACTION (action), group);
	    }
	  else
	    g_warning (G_STRLOC " could not look up `%s'", entries[i].extra_data);
	}

      if (entries[i].callback)
	g_signal_connect (action, "activate",
			  entries[i].callback, entries[i].user_data);

      /* set the accel path for the menu item */
      accel_path = g_strconcat ("<Actions>/", store->name, "/", entries[i].name, NULL);
      if (entries[i].accelerator)
	{
	  guint accel_key = 0;
	  GdkModifierType accel_mods;

	  gtk_accelerator_parse (entries[i].accelerator, &accel_key,
				 &accel_mods);
	  if (accel_key)
	    gtk_accel_map_add_entry (accel_path, accel_key, accel_mods);
	}

      gmenu_action_set_accel_path (action, accel_path);
      g_free(accel_path);

      gmenu_action_store_add_action (store, action);
      g_object_unref (action);
    }
}
