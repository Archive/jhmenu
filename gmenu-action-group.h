#ifndef GMENU_ACTION_GROUP_H
#define GMENU_ACTION_GROUP_H

#include <gtk/gtk.h>
#include "gmenu-action.h"

#define GMENU_TYPE_ACTION_GROUP              (gmenu_action_group_get_type ())
#define GMENU_ACTION_GROUP(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_ACTION_GROUP, GMenuActionGroup))
#define GMENU_ACTION_GROUP_CLASS(vtable)     (G_TYPE_CHECK_CLASS_CAST ((vtable), GMENU_TYPE_ACTION_GROUP, GMenuActionGroupClass))
#define GMENU_IS_ACTION_GROUP(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_ACTION_GROUP))
#define GMENU_IS_ACTION_GROUP_CLASS(vtable)  (G_TYPE_CHECK_CLASS_TYPE ((vtable), GMENU_TYPE_ACTION_GROUP))
#define GMENU_ACTION_GROUP_GET_CLASS(inst)   (G_TYPE_INSTANCE_GET_INTERFACE ((inst), GMENU_TYPE_ACTION_GROUP, GMenuActionGroupClass))

typedef struct _GMenuActionGroup      GMenuActionGroup;
typedef struct _GMenuActionGroupClass GMenuActionGroupClass;

struct _GMenuActionGroupClass
{
  GTypeInterface base_iface;

  const gchar *(* get_name)   (GMenuActionGroup *action_group);
  GMenuAction *(* get_action) (GMenuActionGroup *action_group,
			       const gchar *action_name);
};

GType gmenu_action_group_get_type (void);

const gchar *gmenu_action_group_get_name   (GMenuActionGroup *action_group);
GMenuAction *gmenu_action_group_get_action (GMenuActionGroup *action_group,
					    const gchar *action_name);



#endif
