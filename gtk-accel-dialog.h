#ifndef GTK_ACCEL_DIALOG_H
#define GTK_ACCEL_DIALOG_H

#include <gtk/gtk.h>

#define GTK_TYPE_ACCEL_DIALOG            (gtk_accel_dialog_get_type ())
#define GTK_ACCEL_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_ACCEL_DIALOG, GtkAccelDialog))
#define GTK_ACCEL_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_ACCEL_DIALOG, GtkAccelDialogClass))
#define GTK_IS_ACCEL_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_ACCEL_DIALOG))
#define GTK_IS_ACCEL_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GTK_TYPE_ACCEL_DIALOG))
#define GTK_ACCEL_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GTK_TYPE_ACCEL_DIALOG, GtkAccelDialogClass))

typedef struct _GtkAccelDialog      GtkAccelDialog;
typedef struct _GtkAccelDialogClass GtkAccelDialogClass;

struct _GtkAccelDialog {
  GtkDialog parent;

  GtkListStore *accel_store;

  GtkWidget *accel_view;

  GtkWidget *shift_toggle;
  GtkWidget *ctrl_toggle;
  GtkWidget *alt_toggle;
  GtkWidget *key_entry;

  GtkWidget *set_button;
  GtkWidget *reset_button;

  GtkWidget *ok_button;
};

struct _GtkAccelDialogClass {
  GtkDialogClass parent_class;
};

GType      gtk_accel_dialog_get_type      (void);
GtkWidget *gtk_accel_dialog_new           (void);

void       gtk_accel_dialog_rescan_accels (GtkAccelDialog *accel_dialog);

#endif
