#include <string.h>
#include "gmenu-action-stack.h"

#ifndef _
# define _(s) (s)
#endif

static void gmenu_action_stack_init       (GMenuActionStack *action_stack);
static void gmenu_action_stack_class_init (GMenuActionStackClass *class);
static void gmenu_action_stack_group_init (GMenuActionGroupClass *vtable);

GType
gmenu_action_stack_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuActionStackClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gmenu_action_stack_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GMenuActionStack),
        0, /* n_preallocs */
        (GInstanceInitFunc) gmenu_action_stack_init,
      };
      static const GInterfaceInfo group_info =
      {
        (GInterfaceInitFunc) gmenu_action_stack_group_init,
        (GInterfaceFinalizeFunc) NULL,
        NULL,
      };

      type = g_type_register_static (G_TYPE_OBJECT, "GMenuActionStack",
				     &type_info, 0);
      g_type_add_interface_static (type, GMENU_TYPE_ACTION_GROUP, &group_info);
    }

  return type;
}

static void         gmenu_action_stack_finalize   (GObject *object);
static const gchar *gmenu_action_stack_get_name   (GMenuActionGroup *group);
static GMenuAction *gmenu_action_stack_get_action (GMenuActionGroup *group,
						   const gchar *name);
static GObjectClass *parent_class = NULL;

static void
gmenu_action_stack_class_init (GMenuActionStackClass *class)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (class);
  parent_class = g_type_class_peek_parent (class);

  object_class->finalize = gmenu_action_stack_finalize;
}

static void
gmenu_action_stack_group_init (GMenuActionGroupClass *vtable)
{
  vtable->get_name   = gmenu_action_stack_get_name;
  vtable->get_action = gmenu_action_stack_get_action;
}

static void
gmenu_action_stack_init (GMenuActionStack *action_stack)
{
  action_stack->name = NULL;
  action_stack->groups = NULL;
}

static void
gmenu_action_stack_finalize (GObject *object)
{
  GMenuActionStack *action_stack;

  action_stack = GMENU_ACTION_STACK (object);

  g_free (action_stack->name);
  action_stack->name = NULL;

  g_list_foreach(action_stack->groups, (GFunc)g_object_unref, NULL);
  g_list_free(action_stack->groups);
  action_stack->groups = NULL;

  if (parent_class->finalize)
    (* parent_class->finalize) (object);
}

static const gchar *
gmenu_action_stack_get_name (GMenuActionGroup *group)
{
  GMenuActionStack *action_stack;

  action_stack = GMENU_ACTION_STACK (group);

  return action_stack->name;
}

static GMenuAction *
gmenu_action_stack_get_action (GMenuActionGroup *group, const gchar *name)
{
  GMenuActionStack *action_stack;
  const gchar *colon, *action_name;
  gchar *group_name;
  GList *tmp;
  GMenuAction *action = NULL;

  action_stack = GMENU_ACTION_STACK (group);

  colon = strchr(name, ':');
  if (!colon || colon[1] != ':')
    {
      /* go through each action til we find a match */
      for (tmp = action_stack->groups; tmp; tmp = tmp->next)
	{
	  GMenuActionGroup *agroup = tmp->data;

	  action = gmenu_action_group_get_action(agroup, name);
	  if (action)
	    break;
	}
    }
  else
    {
      /* find a group with a matching name and do the lookup there */
      group_name = g_strndup (name, colon - name);
      action_name = &colon[2];
      for (tmp = action_stack->groups; tmp; tmp = tmp->next)
	{
	  GMenuActionGroup *agroup = tmp->data;

	  if (!strcmp(group_name, gmenu_action_group_get_name(agroup)))
	    {
	      action = gmenu_action_group_get_action(agroup, action_name);
	      break;
	    }
	}
    }

  return action;
}

GMenuActionStack *
gmenu_action_stack_new (const gchar *name)
{
  GMenuActionStack *action_stack = g_object_new (GMENU_TYPE_ACTION_STACK,
						 NULL);

  action_stack->name = g_strdup (name);

  return action_stack;
}

void
gmenu_action_stack_insert_group (GMenuActionStack *stack,
				 GMenuActionGroup *group,
				 gint pos)
{
  g_return_if_fail (GMENU_IS_ACTION_STACK (stack));
  g_return_if_fail (GMENU_IS_ACTION_GROUP (group));
  g_return_if_fail (g_list_find(stack->groups, group) == NULL);

  g_object_ref(group);
  stack->groups = g_list_insert(stack->groups, group, pos);
}

void
gmenu_action_stack_remove_group (GMenuActionStack *stack,
				 GMenuActionGroup *group)
{
  g_return_if_fail (GMENU_IS_ACTION_STACK (stack));
  g_return_if_fail (GMENU_IS_ACTION_GROUP (group));
  g_return_if_fail (g_list_find(stack->groups, group) != NULL);

  stack->groups = g_list_remove(stack->groups, group);
  g_object_unref(group);
}
