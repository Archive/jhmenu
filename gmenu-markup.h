#ifndef GMENU_MARKUP_H
#define GMENU_MARKUP_H

#include <gtk/gtk.h>
#include "gmenu-action.h"
#include "gmenu-action-group.h"

typedef void (*GMenuWidgetFunc) (GtkWidget *widget,
				 const gchar *type,
				 const gchar *name,
				 gpointer user_data);

gboolean gmenu_create_from_string (GMenuActionGroup *action_group,
				   GMenuWidgetFunc widget_func,
				   gpointer user_data,
				   GtkAccelGroup *accel_group,
				   const gchar *buffer, guint length,
				   GError **error);

gboolean gmenu_create_from_file   (GMenuActionGroup *action_group,
				   GMenuWidgetFunc widget_func,
				   gpointer user_data,
				   GtkAccelGroup *accel_group,
				   const gchar *filename,
				   GError **error);

#endif
