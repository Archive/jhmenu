#ifndef GMENU_ACTION_STACK_H
#define GMENU_ACTION_STACK_H

#include <gtk/gtk.h>
#include "gmenu-action.h"
#include "gmenu-toggle-action.h"
#include "gmenu-radio-action.h"
#include "gmenu-action-group.h"

#define GMENU_TYPE_ACTION_STACK            (gmenu_action_stack_get_type ())
#define GMENU_ACTION_STACK(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_ACTION_STACK, GMenuActionStack))
#define GMENU_ACTION_STACK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GMENU_TYPE_ACTION_STACK, GMenuActionStackClass))
#define GMENU_IS_ACTION_STACK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_ACTION_STACK))
#define GMENU_IS_ACTION_STACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GMENU_TYPE_ACTION_STACK))
#define GMENU_ACTION_STACK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GMENU_TYPE_ACTION_STACK, GMenuActionStackClass))

typedef struct _GMenuActionStack      GMenuActionStack;
typedef struct _GMenuActionStackClass GMenuActionStackClass;
typedef struct _GMenuActionStackEntry GMenuActionStackEntry;

struct _GMenuActionStack {
  GObject parent;

  gchar *name;
  GList *groups;
};

struct _GMenuActionStackClass {
  GObjectClass parent_class;
};


GType             gmenu_action_stack_get_type      (void);

GMenuActionStack *gmenu_action_stack_new           (const gchar *name);

void              gmenu_action_stack_insert_group  (GMenuActionStack *stack,
						    GMenuActionGroup *group,
						    gint              pos);
void              gmenu_action_stack_remove_group  (GMenuActionStack *stack,
						    GMenuActionGroup *group);

#endif

