#ifndef GMENU_MERGE_H
#define GMENU_MERGE_H

#include <glib.h>
#include <glib-object.h>
#include <gmenu-action.h>
#include <gmenu-action-group.h>

#define GMENU_TYPE_MERGE            (gmenu_merge_get_type ())
#define GMENU_MERGE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_MERGE, GMenuMerge))
#define GMENU_MERGE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GMENU_TYPE_MERGE, GMenuMergeClass))
#define GMENU_IS_MERGE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_MERGE))
#define GMENU_IS_MERGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GMENU_TYPE_MERGE))
#define GMENU_MERGE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GMENU_TYPE_MERGE, GMenuMergeClass))

typedef struct _GMenuMerge      GMenuMerge;
typedef struct _GMenuMergeClass GMenuMergeClass;
typedef struct _GMenuMergeNode  GMenuMergeNode;

typedef enum {
  GMENU_MERGE_UNDECIDED,
  GMENU_MERGE_ROOT,
  GMENU_MERGE_MENUBAR,
  GMENU_MERGE_MENU,
  GMENU_MERGE_TOOLBAR,
  GMENU_MERGE_PLACEHOLDER,
  GMENU_MERGE_POPUPS,
  GMENU_MERGE_MENUITEM,
  GMENU_MERGE_TOOLITEM,
} GMenuMergeNodeType;

struct _GMenuMerge {
  GObject parent;

  GtkAccelGroup *accel_group;

  GNode *root_node;
  GList *action_groups;

  guint update_tag;
};

struct _GMenuMergeClass {
  GObjectClass parent_class;
};

struct _GMenuMergeNode {
  GMenuMergeNodeType type;

  const gchar *name;

  GQuark action_name;
  GMenuAction *action;
  GtkWidget *proxy;

  guint dirty : 1;

  /* need something in here as a list of the UI files that reference
   * this node */
  GSList *uifiles;
};

GType       gmenu_merge_get_type (void);
GMenuMerge *gmenu_merge_new      (void);

/* these two functions will dirty all merge nodes, as they may need to
 * be connected up to different actions */
void gmenu_merge_insert_action_group (GMenuMerge *self,
				      GMenuActionGroup *action_group,
				      gint pos);
void gmenu_merge_remove_action_group (GMenuMerge *self,
				      GMenuActionGroup *action_group);


GtkWidget *gmenu_merge_get_widget (GMenuMerge *self, const gchar *path);

/* these two functions are for adding UI elements to the merged user
 * interface */
gboolean gmenu_merge_add_ui_from_string (GMenuMerge *self,
					 const gchar *buffer, guint length,
					 GError **error);
gboolean gmenu_merge_add_ui_from_file (GMenuMerge *self,
				       const gchar *filename,
				       GError **error);


#endif /* GMENU_MERGE_H */
