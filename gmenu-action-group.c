#include "gmenu-action-group.h"

GType
gmenu_action_group_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuActionGroupClass),  /* class_size */
        NULL,                            /* base_init */
        NULL,                            /* base_finalize */
      };

      type = g_type_register_static (G_TYPE_INTERFACE, "GMenuActionGroup",
				     &type_info, 0);
      g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

  return type;
}

const gchar *
gmenu_action_group_get_name (GMenuActionGroup *action_group)
{
  g_return_val_if_fail (GMENU_IS_ACTION_GROUP (action_group), NULL);

  if (GMENU_ACTION_GROUP_GET_CLASS (action_group)->get_name)
    return (* GMENU_ACTION_GROUP_GET_CLASS (action_group)->get_name)
      (action_group);
  return NULL;
}

GMenuAction *
gmenu_action_group_get_action (GMenuActionGroup *action_group,
			       const gchar *action_name)
{
  g_return_val_if_fail (GMENU_IS_ACTION_GROUP (action_group), NULL);
  g_return_val_if_fail (GMENU_ACTION_GROUP_GET_CLASS (action_group)->get_action != NULL, NULL);

  return (* GMENU_ACTION_GROUP_GET_CLASS (action_group)->get_action)
    (action_group, action_name);
}


