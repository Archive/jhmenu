#ifndef GMENU_RADIO_ACTION_H
#define GMENU_RADIO_ACTION_H

#include <gtk/gtk.h>
#include "gmenu-action.h"
#include "gmenu-toggle-action.h"

#define GMENU_TYPE_RADIO_ACTION            (gmenu_radio_action_get_type ())
#define GMENU_RADIO_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_RADIO_ACTION, GMenuRadioAction))
#define GMENU_RADIO_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GMENU_TYPE_RADIO_ACTION, GMenuRadioActionClass))
#define GMENU_IS_RADIO_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_RADIO_ACTION))
#define GMENU_IS_RADIO_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GMENU_TYPE_RADIO_ACTION))
#define GMENU_RADIO_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GMENU_TYPE_RADIO_ACTION, GMenuRadioActionClass))

typedef struct _GMenuRadioAction      GMenuRadioAction;
typedef struct _GMenuRadioActionClass GMenuRadioActionClass;

struct _GMenuRadioAction {
  GMenuToggleAction parent;

  GSList *group;
};

struct _GMenuRadioActionClass {
  GMenuToggleActionClass parent_class;
};

GType    gmenu_radio_action_get_type  (void);

GSList  *gmenu_radio_action_get_group (GMenuRadioAction *action);
void     gmenu_radio_action_set_group (GMenuRadioAction *action,
				       GSList *group);

#endif
