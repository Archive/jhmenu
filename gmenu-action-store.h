#ifndef GMENU_ACTION_STORE_H
#define GMENU_ACTION_STORE_H

#include <gtk/gtk.h>
#include "gmenu-action.h"
#include "gmenu-toggle-action.h"
#include "gmenu-radio-action.h"
#include "gmenu-action-group.h"

#define GMENU_TYPE_ACTION_STORE            (gmenu_action_store_get_type ())
#define GMENU_ACTION_STORE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_ACTION_STORE, GMenuActionStore))
#define GMENU_ACTION_STORE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GMENU_TYPE_ACTION_STORE, GMenuActionStoreClass))
#define GMENU_IS_ACTION_STORE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_ACTION_STORE))
#define GMENU_IS_ACTION_STORE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GMENU_TYPE_ACTION_STORE))
#define GMENU_ACTION_STORE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GMENU_TYPE_ACTION_STORE, GMenuActionStoreClass))

typedef struct _GMenuActionStore      GMenuActionStore;
typedef struct _GMenuActionStoreClass GMenuActionStoreClass;
typedef struct _GMenuActionStoreEntry GMenuActionStoreEntry;

struct _GMenuActionStore {
  GObject parent;

  gchar *name;
  GHashTable *actions;
};

struct _GMenuActionStoreClass {
  GObjectClass parent_class;
};

typedef enum {
  NORMAL_ACTION,
  TOGGLE_ACTION,
  RADIO_ACTION
} GMenuActionStoreEntryType;

struct _GMenuActionStoreEntry {
  gchar *name;
  gchar *label;
  gchar *stock_id;
  gchar *accelerator;
  gchar *tooltip;

  GCallback callback;
  gpointer user_data;

  GMenuActionStoreEntryType entry_type;
  gchar *extra_data;
};

GType             gmenu_action_store_get_type      (void);

GMenuActionStore *gmenu_action_store_new           (const gchar *name);

void              gmenu_action_store_add_action    (GMenuActionStore *store,
						    GMenuAction *action);
void              gmenu_action_store_remove_action (GMenuActionStore *store,
						    GMenuAction *action);

void              gmenu_action_store_add_actions   (GMenuActionStore *store,
						    GMenuActionStoreEntry *entries,
						    guint n_entries);

#endif

