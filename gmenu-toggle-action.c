#include "gmenu-toggle-action.h"

enum {
  TOGGLED,
  LAST_SIGNAL
};

static void gmenu_toggle_action_init       (GMenuToggleAction *action);
static void gmenu_toggle_action_class_init (GMenuToggleActionClass *class);

GType
gmenu_toggle_action_get_type (void)
{
  static GtkType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GMenuToggleActionClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gmenu_toggle_action_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GMenuToggleAction),
        0, /* n_preallocs */
        (GInstanceInitFunc) gmenu_toggle_action_init,
      };

      type = g_type_register_static (GMENU_TYPE_ACTION,
                                     "GMenuToggleAction",
                                     &type_info, 0);
    }
  return type;
}

static void gmenu_toggle_action_activate (GMenuAction *action);
static void gmenu_toggle_action_real_toggled (GMenuToggleAction *action);

static GtkWidget *create_menu_item    (GMenuAction *action);
static GtkWidget *add_toolbar_button  (GMenuAction *action,
				       GtkToolbar *toolbar);

static GObjectClass *parent_class = NULL;
static guint         action_signals[LAST_SIGNAL] = { 0 };

static void
gmenu_toggle_action_class_init (GMenuToggleActionClass *class)
{
  GMenuActionClass *action_class;

  parent_class = g_type_class_peek_parent (class);
  action_class = GMENU_ACTION_CLASS (class);

  action_class->activate = gmenu_toggle_action_activate;
  action_class->create_menu_item = create_menu_item;
  action_class->add_toolbar_button = add_toolbar_button;
  class->toggled = gmenu_toggle_action_real_toggled;

  action_signals[TOGGLED] =
    g_signal_new ("toggled",
                  G_OBJECT_CLASS_TYPE (class),
                  G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (GMenuToggleActionClass, toggled),
		  NULL, NULL,
                  gtk_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);
}

static void
gmenu_toggle_action_init (GMenuToggleAction *action)
{
  action->active = FALSE;
}

static void
gmenu_toggle_action_activate (GMenuAction *action)
{
  GMenuToggleAction *toggle_action;

  g_return_if_fail (GMENU_IS_TOGGLE_ACTION (action));

  toggle_action = GMENU_TOGGLE_ACTION (action);

  toggle_action->active = !toggle_action->active;

  gmenu_toggle_action_toggled (toggle_action);
}

static void
gmenu_toggle_action_real_toggled (GMenuToggleAction *action)
{
  GSList *slist;

  g_return_if_fail (GMENU_IS_TOGGLE_ACTION (action));

  for (slist = GMENU_ACTION (action)->proxies; slist; slist = slist->next)
    {
      GtkWidget *proxy = slist->data;

      gmenu_action_block_activate_from (GMENU_ACTION (action), proxy);
      if (GTK_IS_CHECK_MENU_ITEM (proxy))
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (proxy),
					action->active);
      else if (GTK_IS_TOGGLE_BUTTON (proxy))
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (proxy),
				      action->active);
      else {
	g_warning ("Don't know how to toggle `%s' widgets",
		   G_OBJECT_TYPE_NAME (proxy));
      }
      gmenu_action_unblock_activate_from (GMENU_ACTION (action), proxy);
    }
}

static GtkWidget *
create_menu_item (GMenuAction *action)
{
  GMenuToggleAction *toggle_action;
  GtkWidget *menu_item;
  GtkWidget *label;

  toggle_action = GMENU_TOGGLE_ACTION (action);

  menu_item = gtk_check_menu_item_new ();

  gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu_item),
				  toggle_action->active);

  label = g_object_new (GTK_TYPE_ACCEL_LABEL,
			"label", action->label,
			"use_underline", TRUE,
			"xalign", 0.0,
			"visible", TRUE,
			"parent", menu_item,
			"accel_widget", menu_item,
			NULL);
  gtk_accel_label_refetch (GTK_ACCEL_LABEL (label));
  
  gtk_widget_show (menu_item);
  return menu_item;
}

static void
set_use_underline (GtkWidget *widget, GtkWidget *button)
{
  if (GTK_IS_LABEL (widget)) {
    gtk_label_set_use_underline (GTK_LABEL (widget), TRUE);
    gtk_label_set_mnemonic_widget (GTK_LABEL (widget), button);
  }
}

static GtkWidget *
add_toolbar_button (GMenuAction *action, GtkToolbar *toolbar)
{
  GMenuToggleAction *toggle_action;
  GtkWidget *icon;
  GtkWidget *button;

  toggle_action = GMENU_TOGGLE_ACTION (action);

  icon = gmenu_action_create_icon (action, GTK_ICON_SIZE_SMALL_TOOLBAR);

  button = gtk_toolbar_append_element (toolbar,
				       GTK_TOOLBAR_CHILD_TOGGLEBUTTON,
				       NULL, action->label,
				       action->tooltip, NULL,
				       icon, NULL, NULL);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
				toggle_action->active);

  /* make the label in the toolbar use a mnemonic */
  gtk_container_foreach (GTK_CONTAINER(GTK_BIN (button)->child),
			 (GtkCallback) set_use_underline, button);
  return button;
}

void
gmenu_toggle_action_toggled (GMenuToggleAction *action)
{
  g_return_if_fail (GMENU_IS_TOGGLE_ACTION (action));

  g_signal_emit (action, action_signals[TOGGLED], 0);
}

void
gmenu_toggle_action_set_active (GMenuToggleAction *action, gboolean is_active)
{
  g_return_if_fail (GMENU_IS_TOGGLE_ACTION (action));

  is_active = is_active != 0;

  if (action->active != is_active)
    {
      gmenu_action_activate (GMENU_ACTION (action));
    }
}

gboolean
gmenu_toggle_action_get_active (GMenuToggleAction *action)
{
  g_return_val_if_fail (GMENU_IS_TOGGLE_ACTION (action), FALSE);

  return action->active;
}
