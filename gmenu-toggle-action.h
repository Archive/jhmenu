#ifndef GMENU_TOGGLE_ACTION_H
#define GMENU_TOGGLE_ACTION_H

#include <gtk/gtk.h>
#include "gmenu-action.h"

#define GMENU_TYPE_TOGGLE_ACTION            (gmenu_toggle_action_get_type ())
#define GMENU_TOGGLE_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GMENU_TYPE_TOGGLE_ACTION, GMenuToggleAction))
#define GMENU_TOGGLE_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GMENU_TYPE_TOGGLE_ACTION, GMenuToggleActionClass))
#define GMENU_IS_TOGGLE_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMENU_TYPE_TOGGLE_ACTION))
#define GMENU_IS_TOGGLE_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GMENU_TYPE_TOGGLE_ACTION))
#define GMENU_TOGGLE_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GMENU_TYPE_TOGGLE_ACTION, GMenuToggleActionClass))

typedef struct _GMenuToggleAction      GMenuToggleAction;
typedef struct _GMenuToggleActionClass GMenuToggleActionClass;

struct _GMenuToggleAction {
  GMenuAction parent;

  guint active : 1;
};

struct _GMenuToggleActionClass {
  GMenuActionClass parent_class;

  void (* toggled) (GMenuToggleAction *action);
};

GType    gmenu_toggle_action_get_type   (void);

void     gmenu_toggle_action_toggled    (GMenuToggleAction *action);
void     gmenu_toggle_action_set_active (GMenuToggleAction *action,
				         gboolean is_active);
gboolean gmenu_toggle_action_get_active (GMenuToggleAction *action);

#endif
